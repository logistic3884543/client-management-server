# AI-Powered Customer Interaction and Recommendation System

This project is an AI-powered web application designed to enhance customer interaction and generate personalized recommendations using OpenAI's GPT-3 technology. It provides a seamless interface for user registration, login, and managing customer queries, leveraging the power of machine learning to offer tailored advice and responses.

## Features

- **User Authentication**: Secure login and registration functionality for users.
- **Customer Management**: Ability to submit and manage customer inquiries and feedback.
- **AI-Driven Recommendations**: Utilizes GPT-3 to generate personalized recommendations based on customer interactions.

## Technologies Used

- Node.js and Express for the backend.
- MongoDB for the database.
- OpenAI's GPT-3 for generating recommendations.
- React (suggested for frontend, not included in the uploaded files).

## Getting Started

### Prerequisites

- Node.js installed.
- MongoDB account and database set up.
- OpenAI API key.

### Installation

1. Clone the repository and navigate into it.
2. Install dependencies: `npm install`.
3. Set up your `.env` file with your MongoDB URI and OpenAI API key.
4. Start the server: `npm start`.

### Environment Variables

- `MONGODB_URI`: Your MongoDB connection string.
- `OPENAI_API_KEY`: Your OpenAI API key.
- `TOKEN_SECRET`: A secret key for JWT token generation.

## Usage

The server starts on `http://localhost:3000` by default. Use Postman or any HTTP client to interact with the API endpoints:

- `/auth/login` and `/auth/register` for user authentication.
- `/customer/submit` to submit customer inquiries.
- Use headers for authentication with the JWT token received upon login.

## Contributing

Contributions are welcome! Please open an issue or submit a pull request with your proposed changes.

## License

This project is licensed under the MIT License - see the LICENSE file for details.
